<?php
/*
    index.php : contrôleur frontal (front-controller)
        fichier appelé à chaque requête HTTP
*/
// auto-chargement des classes avec composer
require_once '../vendor/autoload.php';

// import des classes

use App\Core\Container;
use App\Core\Database;
use App\Core\Dotenv;
use App\Core\Routing;
use App\Query\UserQuery;
use App\Service\JWT;

// Chargement du fichier .env
Dotenv::load();

// tests
/*$db = new Database();
var_dump($db->connect()); exit;*/
//$userQuery = Container::getInstance(UserQuery::class);
//echo "<pre>"; var_dump($userQuery->checkUser('admin','admin')); echo"</pre>"; exit;

//$jwt = Container::getInstance(JWT::class);
//echo "<pre>"; var_dump($jwt->generate()); echo"</pre>";



// routage 
//$routing = new Routing();

// propriété statique créée sur tous les objets qui permet de récupérer l'espace de nom sous forme de chaine de caractère
$routing = Container::getInstance(Routing::class);
$routeInfos = $routing->getRouteInfos();

//echo "<pre>"; var_dump($routeInfos); echo "</pre>"; exit;

// espace de nom du contrôleur : App\Controller\
$controllerName = "App\Controller\\{$routeInfos['controller']}";

// instanciation du contrôleur 
$controller = Container::getInstance($controllerName);

// appel de la méthode
// ... spread opérator permet de convertir des tableaux associatifs en arguments
// ['id' => 50] > id=50,
$controller->{$routeInfos['method']}($routeInfos["vars"]);


