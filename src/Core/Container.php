<?php

namespace App\Core;

use App\Controller\Authentification;
use App\Controller\Homepage;
use App\Controller\NotFound;
use App\Model\User;
use App\Query\UserQuery;
use App\Service\JWT;

/*
    conteneurs d'instances des classes utilisées dans l'application cette classe renvoie les instances
*/

class Container
{
    static public function getInstance(String $namespace)
    {
        /*
            une fonction est utilisée pour différer l'instanciation 
        */
        $instances = [
            NotFound::class => function (){ 
                return new \App\Controller\NotFound();
            },
            Homepage::class => function (){ 
                return new \App\Controller\Homepage();
            },
            Authentification::class => function (){ 
                return new \App\Controller\Authentification();
            },
            Routing::class => function (){ 
                return new \App\Core\Routing();
            },
            Database::class => function(){
                return new \App\Core\Database();
            },
            Dotenv::class => function(){
                return new \App\Core\Dotenv();
            },
            User::class => function(){
                return new \App\Model\User();
            },
            UserQuery::class => function(){
                return new \App\Query\UserQuery(self::getInstance(Database::class),);
            },
            JWT::class => function(){
                return new \App\Service\JWT();
            },
        ];

        return $instances[$namespace]();
    }
}